﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventory_Project_Milestone_3
{
    public partial class Form1 : Form
    {
        string[] itemArray = new string[100];
        Inventory_Manager inv = new Inventory_Manager();
        Inventory_Item storedEdit = new Inventory_Item();
        public Form1()
        {
            InitializeComponent();
            itemArray = listBox1.Items.OfType<string>().ToArray();
            listBox1.SetSelected(0, true);
            foreach (var items in itemArray)
            {
                inv.Add(items);
            }
            Selection();

        }

        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CanEdit(false);
                Selection();
            }
            catch (Exception ex) { }



        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            int productnum = 0;
            CanEdit(false);
            if (int.TryParse(searchBox.Text, out productnum))
            {
                try { listBox1.SelectedItem = inv.Search(productnum).Name; }
                catch (Exception ex){ MessageBox.Show("Please enter a valid 9 digit product #."); }

            }
            else
            {
                try
                { listBox1.SelectedItem = inv.Search(searchBox.Text).Name; }
                catch (Exception ex) { MessageBox.Show("Please enter a valid inventory name."); }
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            CanEdit(false);
            inv.Add(addBox.Text);
            listBox1.Items.Add(addBox.Text);
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            inv.Remove(listBox1.SelectedItem.ToString());
            listBox1.Items.Remove(listBox1.SelectedItem);
            listBox1.SetSelected(0, true);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Inventory_Item selected = new Inventory_Item();
            selected = inv.Search(listBox1.SelectedItem.ToString());
            inv.Restock(listBox1.SelectedItem.ToString());
            stockBox.Text = selected.Stock + "";
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            CanEdit(true);
            storedEdit = CurrentEdit();

        }


        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            CanEdit(false);
        }

        private void AddBox_TextChanged(object sender, EventArgs e)
        {
            CanEdit(false);
        }

        private void CheckBox_Click(object sender, EventArgs e)
        {
            try
            {
                Inventory_Item xyz = new Inventory_Item();
                xyz.Name = listBox1.SelectedItem.ToString();
                xyz.Description = descBox.Text;
                xyz.Expdate = expBox.Text;
                xyz.Fullstock = int.Parse(fullBox.Text);
                xyz.Pnum = int.Parse(pBox.Text);
                xyz.Price = decimal.Parse(priceBox.Text);
                xyz.Stock = int.Parse(stockBox.Text);
                inv.Replace(listBox1.SelectedItem.ToString(), xyz);
                    pBox.Text = xyz.Pnum + "";
                stockBox.Text = xyz.Stock + "";
                 fullBox.Text = xyz.Fullstock + "";
                priceBox.Text = xyz.Price + "";
                  expBox.Text = xyz.Expdate;
                 descBox.Text = xyz.Description;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            CanEdit(false);

        }

        private void XBox_Click(object sender, EventArgs e)
        {
                pBox.Text = storedEdit.Pnum + "";
            stockBox.Text = storedEdit.Stock + "";
             fullBox.Text = storedEdit.Fullstock + "";
            priceBox.Text = storedEdit.Price + "";
              expBox.Text = storedEdit.Expdate;
             descBox.Text = storedEdit.Description;
            CanEdit(false);

        }

        //custom classes
        private void CanEdit(bool tf)
        {

            if (tf == true)
            {
                pBox.ReadOnly = false;
                stockBox.ReadOnly = false;
                fullBox.ReadOnly = false;
                priceBox.ReadOnly = false;
                expBox.ReadOnly = false;
                descBox.ReadOnly = false;
                checkBox.Visible = true;
                xBox.Visible = true;
            }

            else
            {
                pBox.ReadOnly = true;
                stockBox.ReadOnly = true;
                fullBox.ReadOnly = true;
                priceBox.ReadOnly = true;
                expBox.ReadOnly = true;
                descBox.ReadOnly = true;
                checkBox.Visible = false;
                xBox.Visible = false;
            }
        }

        private Inventory_Item CurrentEdit()
        {
            try
            {
                Inventory_Item xyz = new Inventory_Item();
                xyz.Name = listBox1.SelectedItem.ToString();
                xyz.Description = descBox.Text;
                xyz.Expdate = expBox.Text;
                xyz.Fullstock = int.Parse(fullBox.Text);
                xyz.Pnum = int.Parse(pBox.Text);
                xyz.Price = decimal.Parse(priceBox.Text);
                xyz.Stock = int.Parse(stockBox.Text);
                return xyz;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return null;

        }

        private void Selection()
        {
            Inventory_Item selected = new Inventory_Item();
            selected = inv.Search(listBox1.SelectedItem.ToString());
            pBox.Text = selected.Pnum + "";
            stockBox.Text = selected.Stock + "";
            fullBox.Text = selected.Fullstock + "";
            priceBox.Text = selected.Price + "";
            expBox.Text = selected.Expdate;
            descBox.Text = selected.Description;
        }
    }
}














/*
Inventory_Item phone = new Inventory_Item();
Inventory_Item wallet = new Inventory_Item();
Inventory_Item key = new Inventory_Item();
*/


/*switch (listBox1.SelectedItem.ToString())
{
    case "iPhone 5s":
        pBox.Text = phone.Pnum + "";
        stockBox.Text = phone.Stock + "";
        fullBox.Text = phone.Fullstock + "";
        priceBox.Text = phone.Price + "";
        expBox.Text = phone.Expdate;
        descBox.Text = phone.Description;
        break;
    case "Gucci Wallet":
        pBox.Text = wallet.Pnum + "";
        stockBox.Text = wallet.Stock + "";
        fullBox.Text = wallet.Fullstock + "";
        priceBox.Text = wallet.Price + "";
        expBox.Text = wallet.Expdate;
        descBox.Text = wallet.Description;
        break;
    case "Ferrari Key":
        pBox.Text = key.Pnum + "";
        stockBox.Text = key.Stock + "";
        fullBox.Text = key.Fullstock + "";
        priceBox.Text = key.Price + "";
        expBox.Text = key.Expdate;
        descBox.Text = key.Description;
        break;

}*/