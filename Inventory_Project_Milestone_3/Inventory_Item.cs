﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory_Project_Milestone_3
{
    class Inventory_Item
    {
        private string _name;
        private int _pnum;
        private int _stock;
        private int _fullstock;
        private decimal _price;
        private string _expdate;
        private string _description;

        public Inventory_Item()
        {
            _name = "";
            _pnum = 0;
            _stock = 0;
            _fullstock = 0;
            _price = 0.00m;
            _expdate = "";
            _description = "";
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Expdate
        {
            get { return _expdate; }
            set { _expdate = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public int Pnum
        {
            get { return _pnum; }
            set { _pnum = value; }
        }
        public int Stock
        {
            get { return _stock; }
            set { _stock = value; }
        }

        public int Fullstock
        {
            get { return _fullstock; }
            set { _fullstock = value; }
        }
        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }
    }
}
