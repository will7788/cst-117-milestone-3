﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory_Project_Milestone_3
{
    class Inventory_Manager
    {
        private Inventory_Item[] invArray = new Inventory_Item[200];
        private int length = 0;
        private Random rand = new Random();

        public void Add(Inventory_Item xxx) {

            invArray[length] = xxx;
            length++;
        }
        //add string
        public void Add(string x) {
            Inventory_Item tmp = new Inventory_Item();
            tmp.Name = x;
            tmp.Description = "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.";
            tmp.Expdate = "00/00/0000";
            tmp.Fullstock = 100;
            tmp.Pnum = rand.Next(100000000, 999999999);
            tmp.Price = 0.00m;
            tmp.Stock = rand.Next(100);
            invArray[length] = tmp;
            length++;

        }

        public void Replace(string x, Inventory_Item y) {
            if (length > 0)
            {
                for (int i = 0; i < length; i++)
                {
                    if (invArray[i].Name.Equals(x))
                    {
                        invArray[i] = y;
                    }
                }

            }
        }

        //remove based on name
        public void Remove(string x) {
            if (length > 0)
            {
                for (int i = 0; i < length; i++)
                {
                    if (invArray[i].Name.Equals(x))
                    {
                        for (int j = i; j < length; j++)
                        {
                            invArray[j + 1] = invArray[j];
                        }
                    }
                }

            }
        }

        //restock function

        public void Restock(string x)
        {
            Inventory_Item restock = this.Search(x);
            if (invArray.Contains(restock))
            {
                restock.Stock = restock.Fullstock;
            }
        }

        //search by name
        public Inventory_Item Search(string x)
        {
            for (int i = 0; i < length; i++)
            {
                if (invArray[i].Name.Equals(x))
                {
                    return invArray[i];
                }
            }
            return null;
        }

        //search by product #
        public Inventory_Item Search(int x)
        {
            for (int i = 0; i < length; i++)
            {
                if (invArray[i].Pnum == x)
                {
                    return invArray[i];
                }
            }
            return null;
        }

    }



}
